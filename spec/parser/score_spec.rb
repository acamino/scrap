require 'nokogiri'
require_relative '../../parser/score'

describe Parser::Score do
  describe '.parse' do
    context 'when the xml contains a score' do
      it 'returns the score' do
        html = <<-HTML
          <tr>
            <td/>
            <td><span class="score">5 point</span></td>
          </tr>
        HTML
        target = Nokogiri::XML(html).xpath('//tr')
        expect(described_class.parse(target)).to eq(score: 5)
      end
    end

    context 'when the xml does not contain a score' do
      it 'returns an empty object' do
        target = Nokogiri::XML('<tr><td></td><tr>').xpath('//tr')
        expect(described_class.parse(target)).to eq({})
      end
    end
  end
end
