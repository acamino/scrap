require_relative '../../parser/comments'

describe Parser::Base do
  describe '.drop_suffix' do
    context 'for points' do
      context 'when it contains 1 point' do
        it 'drops the suffix' do
          expect(described_class.drop_suffix('1 point')).to eq('1')
        end
      end

      context 'when it contains more than 1 point' do
        it 'drops the suffix' do
          expect(described_class.drop_suffix('4 points')).to eq('4')
        end
      end
    end

    context 'for comments' do
      context 'when it contains 1 comment' do
        it 'drops the suffix' do
          expect(described_class.drop_suffix('1comment')).to eq('1')
        end
      end

      context 'when it contains more than 1 comment' do
        it 'drops the suffix' do
          expect(described_class.drop_suffix('4comments')).to eq('4')
        end
      end
    end
  end
end
