require 'nokogiri'
require_relative '../../parser/comments'

describe Parser::Comments do
  describe '.parse' do
    context 'when the xml contains comments' do
      it 'returns the score' do
        html = <<-HTML
          <tr>
            <td/>
            <td>
              <a/>
              <a/>
              <a>10 comments</a>
            </td>
          </tr>
        HTML
        target = Nokogiri::XML(html).xpath('//tr')
        expect(described_class.parse(target)).to eq(comments: 10)
      end
    end

    context 'when the xml does not contain a score' do
      it 'returns an empty object' do
        target = Nokogiri::XML('<tr><td></td><tr>').xpath('//tr')
        expect(described_class.parse(target)).to eq({})
      end
    end
  end
end
