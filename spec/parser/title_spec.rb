require 'nokogiri'
require_relative '../../parser/title'

describe Parser::Title do
  describe '.parse' do
    context 'when the xml contains a title' do
      it 'returns the title' do
        html = <<-HTML
          <tr>
            <td/>
            <td/>
            <td><a>the title</a></td>
          </tr>
        HTML
        target = Nokogiri::XML(html).xpath('//tr')
        expect(described_class.parse(target)).to eq(title: 'the title')
      end
    end

    context 'when the xml does not contain a title' do
      it 'returns an empty object' do
        target = Nokogiri::XML('<tr><td></td><tr>').xpath('//tr')
        expect(described_class.parse(target)).to eq({})
      end
    end
  end
end
