require_relative '../scrapper'

describe Scrapper do
  describe '#run' do
    subject(:scrapper) { described_class.new.run }

    it "returns a list of 30 news" do
      expect(subject.length).to eq(30)
    end

    it "returns news which contains title, comments, and score" do
      expect(subject.first.keys).to match_array([:title, :comments, :score])
    end
  end
end
