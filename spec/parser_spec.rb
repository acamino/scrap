require 'nokogiri'
require_relative '../parser'

describe Parser do
  describe '#parser' do
    it "parses the document" do
      html = <<-HTML
        <tr>
          <td/>
          <td>
            <span class="score">5 point</span>
            <a/>
            <a/>
            <a>10 comments</a>
          </td>
          <td><a>the title</a></td>
        </tr>
      HTML

      document = Nokogiri::XML(html).xpath('//tr')
      expect(described_class.new(document).parse)
        .to eq(comments: 10, score: 5, title: 'the title')
    end
  end
end
