require_relative './base'

class Parser
  class Comments < Base
    def self.parse(xml)
      comments = drop_suffix(xml.xpath('td[2]/a[3]').text)
      if comments.empty?
        {}
      else
        { comments: comments.to_i }
      end
    end
  end
end
