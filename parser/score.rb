require_relative './base'

class Parser
  class Score < Base
    def self.parse(xml)
      score = drop_suffix(xml.xpath('td[2]/span[@class="score"]').text)
      if score.empty?
        {}
      else
        { score: score.to_i }
      end
    end
  end
end
