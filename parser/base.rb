class Parser
  class Base
    def self.drop_suffix(value)
      points_suffix = /[[:space:]]points?/
      if value =~ points_suffix
        value.gsub(points_suffix, '')
      else
        value.gsub(/comments?/, '')
      end
    end
  end
end
