class Parser
  class Title
    def self.parse(xml)
      title = xml.xpath('td[3]/a').text
      if title.empty?
        {}
      else
        { title: title }
      end
    end
  end
end
