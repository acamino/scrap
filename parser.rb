require_relative './parser/title'
require_relative './parser/score'
require_relative './parser/comments'

class Parser
  PARSERS = [
    Parser::Title,
    Parser::Score,
    Parser::Comments
  ]

  def initialize(document)
    @document = document
  end

  def parse
    PARSERS.reduce({}) do |result, parser|
      result.merge(parser.parse(@document))
    end
  end
end
