require 'nokogiri'
require 'open-uri'
require_relative './parser'

class Scrapper
  def initialize
    @document = Nokogiri::XML(contents)
  end

  def run
    rows = @document.xpath('//table[@class="itemlist"]/tr[@class="athing"]')
    rows.map { |row| parse(row) }
  end

  private

  def contents
    open('https://news.ycombinator.com/')
  end

  def parse(row)
    title = Parser.new(row).parse
    body = Parser.new(row.next).parse
    title.merge(body)
  end
end
