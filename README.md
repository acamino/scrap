# Scrapper

Grabs the news published at [Hacker News](https://news.ycombinator.com/). It
uses a strategy pattern to describe the different parsers required to parse the
news content.
